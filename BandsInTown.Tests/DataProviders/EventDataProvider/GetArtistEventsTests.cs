﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using BandsInTown.Data.Contracts.DataProviders;
using BandsInTown.Data.Contracts.Models;
using Moq;
using Moq.Protected;
using NUnit.Framework;

namespace BandsInTown.Tests.DataProviders.EventDataProvider
{
  public class GetArtistEventsTests
  {
    [Test]
    public async Task ReturnsEventsWithArtistAndVenueIfEverythingOk()
    {
      var handlerMock = new Mock<HttpMessageHandler>(MockBehavior.Strict);
      var testData = await DataHelper.GetEventTestData();
      handlerMock.Protected().Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(),
          ItExpr.IsAny<CancellationToken>())
        .ReturnsAsync(new HttpResponseMessage
        {
          StatusCode = HttpStatusCode.OK,
          Content = new StringContent(testData)
        }).Verifiable();
      var httpClient = new HttpClient(handlerMock.Object);
      IEventDataProvider repository = new Data.Implementations.DataProviders.Bit.EventDataProvider("11", httpClient);

      var events = await repository.GetArtistEvents("eminem", EType.All);

      handlerMock.Protected().Verify("SendAsync", Times.Exactly(1),
        ItExpr.Is<HttpRequestMessage>(req => req.Method == HttpMethod.Get), ItExpr.IsAny<CancellationToken>()
      );
      Assert.That(events, Is.Not.Null.And.Not.Empty);
      Assert.That(events, Is.All.Matches<Event>(e => IsArtistValid(e?.Artist)),
        "not all events contain full information about the artist");
      Assert.That(events, Is.All.Matches<Event>(e => IsVenueValid(e?.Venue)),
        "not all events contain full information about the venue");
      Assert.That(events, Is.All.Matches<Event>(e => e.Description != null),
        "not all events contain description");
      Assert.That(events, Is.All.Matches<Event>(e => e.Id > 0),
        "not all events contain identifiers");
      Assert.That(events, Is.All.Matches<Event>(e => e?.Url?.IsWellFormedOriginalString() == true),
        "not all events contain their url");
      Assert.That(events, Is.All.Matches<Event>(e => e.Datetime != default), "not all events contain a date");
    }

    private static bool IsArtistValid(Artist artist)
    {
      return !string.IsNullOrWhiteSpace(artist?.Name) && artist.ImageUrl?.IsWellFormedOriginalString() == true &&
             artist.Id > 0;
    }

    private static bool IsVenueValid(Venue venue)
    {
      return !string.IsNullOrWhiteSpace(venue?.City) && !string.IsNullOrWhiteSpace(venue.Country) &&
             !string.IsNullOrWhiteSpace(venue.Name);
    }

    [Test]
    public async Task ReturnsEmptyCollectionIfServerProvideEmptyString()
    {
      var handlerMock = new Mock<HttpMessageHandler>(MockBehavior.Strict);
      handlerMock.Protected().Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(),
          ItExpr.IsAny<CancellationToken>())
        .ReturnsAsync(new HttpResponseMessage
        {
          StatusCode = HttpStatusCode.OK,
          Content = new StringContent("")
        }).Verifiable();
      var httpClient = new HttpClient(handlerMock.Object);
      IEventDataProvider repository = new Data.Implementations.DataProviders.Bit.EventDataProvider("11", httpClient);

      var events = await repository.GetArtistEvents("eminem", EType.All);

      handlerMock.Protected().Verify("SendAsync", Times.Exactly(1),
        ItExpr.Is<HttpRequestMessage>(req => req.Method == HttpMethod.Get), ItExpr.IsAny<CancellationToken>()
      );
      Assert.IsNotNull(events);
      Assert.IsEmpty(events);
    }

    [Test]
    public void ThrowsExceptionWhenAppIdIsInvalid()
    {
      var handlerMock = new Mock<HttpMessageHandler>(MockBehavior.Strict);
      handlerMock.Protected().Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(),
          ItExpr.IsAny<CancellationToken>())
        .ReturnsAsync(new HttpResponseMessage
        {
          StatusCode = HttpStatusCode.Forbidden,
          Content = new StringContent("{\"Message\": \"User is not authorized to access this resource\"}")
        }).Verifiable();
      var httpClient = new HttpClient(handlerMock.Object);
      IEventDataProvider repository = new Data.Implementations.DataProviders.Bit.EventDataProvider("22", httpClient);

      var exception = Assert.ThrowsAsync<Exception>(() =>
        repository.GetArtistEvents("eminem", EType.All));

      Assert.That(exception.Message, Contains.Substring("User is not authorized to access this resource"));
    }

    [Test]
    public void ThrowsArgumentExceptionWhenAppIdIsNullOrWhitespace()
    {
      Assert.Throws<ArgumentException>(() =>
      {
        var _ = new Data.Implementations.DataProviders.Bit.EventDataProvider(null, new HttpClient());
      });
    }

    [Test]
    public void ThrowsArgumentNullExceptionWhenClientIsNull()
    {
      Assert.Throws<ArgumentNullException>(() =>
      {
        var _ = new Data.Implementations.DataProviders.Bit.EventDataProvider("11", null);
      });
    }
  }
}