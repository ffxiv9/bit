﻿using System.IO;
using System.Threading.Tasks;

namespace BandsInTown.Tests
{
  public static class DataHelper
  {
    public static async Task<string> GetEventTestData()
    {
      return await File.ReadAllTextAsync("events.json").ConfigureAwait(false);
    }
  }
}