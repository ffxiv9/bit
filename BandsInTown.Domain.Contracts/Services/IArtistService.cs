﻿using System.Threading.Tasks;
using BandsInTown.Data.Contracts.Models;

namespace BandsInTown.Domain.Contracts.Services
{
  public interface IArtistService
  {
    Task<Venue> GetFavoriteConcertVenue(string artistName);
  }
}