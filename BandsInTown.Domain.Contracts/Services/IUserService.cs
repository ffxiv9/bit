﻿using System.Threading.Tasks;
using BandsInTown.Data.Contracts.Models;

namespace BandsInTown.Domain.Contracts.Services
{
  public interface IUserService
  {
    /// <returns>User if credentials are valid, otherwise returns null</returns>
    Task<User> ValidateUserPassword(string username, string password);
  }
}