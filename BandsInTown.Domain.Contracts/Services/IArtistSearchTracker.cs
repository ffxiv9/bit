﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace BandsInTown.Domain.Contracts.Services
{
  public interface IArtistSearchTracker
  {
    Task Push(string artistName, int userId);
    Task<IEnumerable<string>> GetLastSearches(int userId, int count = 10);
  }
}