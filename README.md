BandsInTown-Web Api
==========================
Web API Service with 2 Endpoints: 

1. /api/artist/{artist_name}, that goes to https://rest.bandsintown.com/artists/{artist_name}/events with requested artist_name and return "the most favourite" artist's place to perform. Response is one string formated as 'City, Country'.
2. /api/history/ URL, where you could receive artists' names from your last 10 searches (requests to the 1st Endpoint).


### Build

``` 
# Restores the dependencies and tools of a project
dotnet restore $PROJECT_NAME

# Builds a project and all of its dependencies.
dotnet build --no-restore $PROJECT_NAME -c $CONFIGURATION
```

## Running the sample
After cloning or downloading the sample you should be able to run it using an SqlLite database immediately. 
You can use a test database file `database.db` and user `admin:admin` to test this application.

## Authentication
``` https://localhost:5001/account/login ```

![Authentication](Images/Authentication.png)

## "the most favourite" artist's place to perform
``` https://localhost:5001/api/artist/{artistName} ```

![ArtistEndPoint](Images/ArtistEndPoint.png)

## Artists' names from your last 10 searches
``` https://localhost:5001/api/history ```

![History](Images/History.png)

```javascript
[
    "Easy Life",
    "Hang Massive",
    "Rare Essence",
    "bbno$",
    "Sasha Sloan",
    "Adam Harvey",
    "Ashnikko",
    "BENEE",
    "Novo Amor",
    "Lauren Sanderson"
]
```
