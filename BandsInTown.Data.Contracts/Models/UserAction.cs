﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BandsInTown.Data.Contracts.Models
{
  public class UserAction
  {
    public int Id { get; set; }
    public DateTime Time { get; set; }
    [Required(AllowEmptyStrings = false)] public string Query { get; set; }
    public virtual User User { get; set; }
    public int UserId { get; set; }
  }
}