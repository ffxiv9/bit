﻿namespace BandsInTown.Data.Contracts.Models
{
  public class Venue
  {
    public string Name { get; set; }
    public float Latitude { get; set; }
    public float Longitude { get; set; }
    public string City { get; set; }
    public string Country { get; set; }
  }
}