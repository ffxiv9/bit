﻿using System;

namespace BandsInTown.Data.Contracts.Models
{
  public class Event
  {
    public int Id { get; set; }
    public Artist Artist { get; set; }
    public Uri Url { get; set; }
    public DateTime Datetime { get; set; }
    public string Description { get; set; }
    public Venue Venue { get; set; }
  }
}