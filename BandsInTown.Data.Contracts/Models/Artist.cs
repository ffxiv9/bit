﻿using System;

namespace BandsInTown.Data.Contracts.Models
{
  public class Artist
  {
    public int Id { get; set; }
    public string Name { get; set; }
    public Uri ImageUrl { get; set; }
  }
}