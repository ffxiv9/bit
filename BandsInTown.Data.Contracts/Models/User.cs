﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BandsInTown.Data.Contracts.Models
{
  public class User
  {
    public int Id { get; set; }

    [Required(AllowEmptyStrings = false)]
    [StringLength(64, MinimumLength = 3)]
    public string Username { get; set; }

    [Required(AllowEmptyStrings = false)] public string Password { get; set; }
    public virtual ICollection<UserAction> UserActions { get; set; }
  }
}