﻿namespace BandsInTown.Data.Contracts.Models
{
  public enum EType
  {
    Upcoming,
    Past,
    All
  }
}