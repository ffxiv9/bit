﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BandsInTown.Data.Contracts.Models;

namespace BandsInTown.Data.Contracts.DataProviders
{
  public interface IEventDataProvider
  {
    Task<List<Event>> GetArtistEvents(string artistName, EType type);
  }
}