﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using BandsInTown.Data.Contracts.DataProviders;
using BandsInTown.Data.Contracts.Models;
using BandsInTown.Data.Implementations.DataProviders.Bit.Responses;
using Newtonsoft.Json;

namespace BandsInTown.Data.Implementations.DataProviders.Bit
{
  public class EventDataProvider : IEventDataProvider
  {
    private const string BaseUrl = "https://rest.bandsintown.com";
    private readonly string _appId;
    private readonly HttpClient _client;

    public EventDataProvider(string appId, HttpClient client)
    {
      _appId = appId ?? throw new ArgumentException("Value cannot be null or whitespace.", nameof(appId));
      _client = client ?? throw new ArgumentNullException(nameof(client));

      _client.DefaultRequestHeaders.Accept.Clear();
      _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
      _client.DefaultRequestHeaders.Add("User-Agent", ".NET Client");
    }

    public async Task<List<Event>> GetArtistEvents(string artistName, EType typeOfEvent)
    {
      var encodedArtistName = HttpUtility.UrlEncode(artistName);
      using var request =
        new HttpRequestMessage(HttpMethod.Get,
          $"/artists/{encodedArtistName}/events?date={typeOfEvent.ToString().ToLower()}");
      var events = await Execute<List<EventResponse>>(request).ConfigureAwait(false);
      if (events == null) return new List<Event>();

      //the date provider specifies the artist only in the first event
      //so we have to set it ourselves in other events
      var artist = events.FirstOrDefault(e => e.Artist != null)?.Artist;
      if (artist != null)
        foreach (var e in events)
          e.Artist = artist;

      return events.Select(e => new Event
      {
        Datetime = e.Datetime,
        Description = e.Description,
        Url = new Uri(e.Url),
        Id = e.Id,
        Artist = new Artist
        {
          Id = e.Artist.Id,
          Name = e.Artist.Name,
          ImageUrl = new Uri(e.Artist.ImageUrl)
        },
        Venue = new Venue
        {
          City = e.VenueResponse.City,
          Country = e.VenueResponse.Country,
          Latitude = e.VenueResponse.Latitude,
          Longitude = e.VenueResponse.Longitude,
          Name = e.VenueResponse.Name
        }
      }).ToList();
    }

    private async Task<T> Execute<T>(HttpRequestMessage request) where T : class, new()
    {
      ProcessUrl(request);

      var content = string.Empty;
      try
      {
        var response = await _client.SendAsync(request).ConfigureAwait(false);
        content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);

        return JsonConvert.DeserializeObject<T>(content);
      }
      catch (Exception) when (!string.IsNullOrWhiteSpace(content))
      {
        TryGetErrorMessage(content);
        throw;
      }
    }

    private static void TryGetErrorMessage(string responseContent)
    {
      try
      {
        var errorMessage = JsonConvert.DeserializeObject<ErrorMessageResponse>(responseContent);
        //BIT provider has 2 different fields for the error message
        var message = errorMessage?.ErrorMessage ?? errorMessage?.Message;
        if (!string.IsNullOrWhiteSpace(message))
          throw new Exception(message);
      }
      catch (JsonException)
      {
        //ignored
      }
    }

    private void ProcessUrl(HttpRequestMessage request)
    {
      var requestUriString = request?.RequestUri?.OriginalString;
      if (string.IsNullOrWhiteSpace(requestUriString))
        throw new ArgumentNullException(nameof(requestUriString));

      var idx = requestUriString.IndexOf('?');
      var requestQueryString = idx >= 0 ? requestUriString.Substring(idx) : string.Empty;
      var requestPathString = idx >= 0 ? requestUriString.Substring(0, idx) : requestUriString;

      var query = HttpUtility.ParseQueryString(requestQueryString);
      query["app_id"] = _appId;

      var uriBuilder = new UriBuilder(BaseUrl) {Query = query.ToString() ?? "", Path = requestPathString};
      request.RequestUri = new Uri(uriBuilder.ToString());
    }
  }
}