﻿using System;
using Newtonsoft.Json;

namespace BandsInTown.Data.Implementations.DataProviders.Bit.Responses
{
  public class ArtistResponse
  {
    [JsonProperty("thumb_url")] public Uri ThumbUrl { get; set; }
    [JsonProperty("support_url")] public string SupportUrl { get; set; }
    [JsonProperty("facebook_page_url")] public string FacebookPageUrl { get; set; }
    [JsonProperty("image_url")] public string ImageUrl { get; set; }
    [JsonProperty("name")] public string Name { get; set; }
    [JsonProperty("id")] public int Id { get; set; }
    [JsonProperty("tracker_count")] public long TrackerCount { get; set; }
    [JsonProperty("upcoming_event_count")] public long UpcomingEventCount { get; set; }
    [JsonProperty("url")] public Uri Url { get; set; }
  }
}