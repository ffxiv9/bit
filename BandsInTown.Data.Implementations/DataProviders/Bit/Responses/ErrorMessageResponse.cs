﻿namespace BandsInTown.Data.Implementations.DataProviders.Bit.Responses
{
  public class ErrorMessageResponse
  {
    public string ErrorMessage { get; set; }
    public string Message { get; set; }
  }
}