﻿using Newtonsoft.Json;

namespace BandsInTown.Data.Implementations.DataProviders.Bit.Responses
{
  public class VenueResponse
  {
    [JsonProperty("name")] public string Name { get; set; }
    [JsonProperty("country")] public string Country { get; set; }
    [JsonProperty("region")] public string Region { get; set; }
    [JsonProperty("city")] public string City { get; set; }
    [JsonProperty("latitude")] public float Latitude { get; set; }
    [JsonProperty("longitude")] public float Longitude { get; set; }
  }
}