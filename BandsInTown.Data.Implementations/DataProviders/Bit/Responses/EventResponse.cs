﻿using System;
using Newtonsoft.Json;

namespace BandsInTown.Data.Implementations.DataProviders.Bit.Responses
{
  public class EventResponse
  {
    [JsonProperty("venue")] public VenueResponse VenueResponse { get; set; }
    [JsonProperty("datetime")] public DateTime Datetime { get; set; }

    [JsonProperty("artist", NullValueHandling = NullValueHandling.Ignore)]
    public ArtistResponse Artist { get; set; }

    [JsonProperty("on_sale_datetime")] public string OnSaleDatetime { get; set; }
    [JsonProperty("description")] public string Description { get; set; }
    [JsonProperty("id")] public int Id { get; set; }
    [JsonProperty("title")] public string Title { get; set; }
    [JsonProperty("artist_id")] public int ArtistId { get; set; }
    [JsonProperty("url")] public string Url { get; set; }
  }
}