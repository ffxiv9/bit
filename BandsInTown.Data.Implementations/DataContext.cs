﻿using BandsInTown.Data.Contracts.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace BandsInTown.Data.Implementations
{
  public class DataContext : DbContext
  {
    public static readonly ILoggerFactory LoggerFactory
      = Microsoft.Extensions.Logging.LoggerFactory.Create(builder =>
      {
        builder
          .AddFilter((category, level) =>
            category == DbLoggerCategory.Database.Command.Name && level == LogLevel.Information).AddDebug();
      });

    protected DataContext()
    {
    }

    public DataContext(DbContextOptions options) : base(options)
    {
    }

    public DbSet<User> Users { get; set; }
    public DbSet<UserAction> UserActions { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
      optionsBuilder.UseLoggerFactory(LoggerFactory);
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
      modelBuilder.Entity<User>().HasIndex(e => e.Username).IsUnique();
    }
  }
}