﻿using System.Collections.Generic;

namespace BandsInTown.Domain.Implementations.Extensions
{
  public static class ListExtensions
  {
    public static List<T> EmptyIfNull<T>(this List<T> list)
    {
      return list ?? new List<T>();
    }
  }
}