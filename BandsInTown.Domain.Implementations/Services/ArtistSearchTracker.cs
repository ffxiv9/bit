﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BandsInTown.Data.Contracts.Models;
using BandsInTown.Data.Implementations;
using BandsInTown.Domain.Contracts.Services;
using BandsInTown.Domain.Implementations.Extensions;
using Microsoft.EntityFrameworkCore;

namespace BandsInTown.Domain.Implementations.Services
{
  public class ArtistSearchTracker : IArtistSearchTracker
  {
    private readonly DataContext _context;

    public ArtistSearchTracker(DataContext context)
    {
      _context = context ?? throw new ArgumentNullException(nameof(context));
    }

    public async Task Push(string artistName, int userId)
    {
      if (string.IsNullOrWhiteSpace(artistName))
        throw new ArgumentException("Value cannot be null or whitespace.", nameof(artistName));
      if (userId <= 0) throw new ArgumentException("Specified value is invalid.", nameof(userId));

      var userAction = new UserAction
      {
        Query = artistName, Time = DateTime.UtcNow, UserId = userId
      };
      await _context.UserActions.AddAsync(userAction);
      await _context.SaveChangesAsync();
    }

    public async Task<IEnumerable<string>> GetLastSearches(int userId, int count = 10)
    {
      if (count <= 0) throw new ArgumentException("Specified value is invalid.", nameof(count));
      if (userId <= 0) throw new ArgumentException("Specified value is invalid.", nameof(userId));

      var userActions = await _context.UserActions.Where(e => e.UserId == userId).OrderByDescending(e => e.Time)
        .Take(count).ToListAsync();
      return userActions.EmptyIfNull().Select(e => e.Query);
    }
  }
}