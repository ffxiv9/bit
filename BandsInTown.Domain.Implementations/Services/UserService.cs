﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using BandsInTown.Data.Contracts.Models;
using BandsInTown.Data.Implementations;
using BandsInTown.Domain.Contracts.Services;
using Microsoft.EntityFrameworkCore;

namespace BandsInTown.Domain.Implementations.Services
{
  public class UserService : IUserService
  {
    private readonly DataContext _context;

    public UserService(DataContext context)
    {
      _context = context ?? throw new ArgumentNullException(nameof(context));
    }

    public Task<User> ValidateUserPassword(string username, string password)
    {
      if (string.IsNullOrWhiteSpace(username))
        throw new ArgumentException("Value cannot be null or whitespace.", nameof(username));
      if (string.IsNullOrWhiteSpace(password))
        throw new ArgumentException("Value cannot be null or whitespace.", nameof(password));

      var encodedPassword = ComputeSha256Hash(password);
      return _context.Users.FirstOrDefaultAsync(user => user.Username == username && user.Password == encodedPassword);
    }

    private static string ComputeSha256Hash(string raw)
    {
      using var sha256Hash = SHA256.Create();
      var bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(raw));

      var builder = new StringBuilder();
      foreach (var b in bytes) builder.Append(b.ToString("x2"));

      return builder.ToString();
    }
  }
}