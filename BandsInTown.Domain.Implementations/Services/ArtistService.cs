﻿using System;
using System.Linq;
using System.Threading.Tasks;
using BandsInTown.Data.Contracts.DataProviders;
using BandsInTown.Data.Contracts.Models;
using BandsInTown.Domain.Contracts.Services;
using BandsInTown.Domain.Implementations.Extensions;

namespace BandsInTown.Domain.Implementations.Services
{
  public class ArtistService : IArtistService
  {
    private readonly IEventDataProvider _eventDataProvider;

    public ArtistService(IEventDataProvider eventDataProvider)
    {
      _eventDataProvider = eventDataProvider ?? throw new ArgumentNullException(nameof(eventDataProvider));
    }

    public async Task<Venue> GetFavoriteConcertVenue(string name)
    {
      if (string.IsNullOrWhiteSpace(name))
        throw new ArgumentException("Value cannot be null or whitespace.", nameof(name));

      var events = await _eventDataProvider.GetArtistEvents(name, EType.All);
      return events.EmptyIfNull().GroupBy(e => new {e.Venue.Country, e.Venue.City}).OrderByDescending(g => g.Count())
        .Select(g => g.First().Venue).FirstOrDefault();
    }
  }
}