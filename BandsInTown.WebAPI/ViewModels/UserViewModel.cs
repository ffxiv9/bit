﻿using System.ComponentModel.DataAnnotations;

namespace BandsInTown.WebAPI.ViewModels
{
  public class UserViewModel
  {
    [Required(AllowEmptyStrings = false)]
    [StringLength(64, MinimumLength = 3)]
    public string Username { get; set; }

    [Required(AllowEmptyStrings = false)] public string Password { get; set; }
  }
}