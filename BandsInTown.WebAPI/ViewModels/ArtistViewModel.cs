﻿using System.ComponentModel.DataAnnotations;

namespace BandsInTown.WebAPI.ViewModels
{
  public class ArtistViewModel
  {
    [Required(AllowEmptyStrings = false, ErrorMessage = "Artist name not specified")]
    //The longest artist name is 51 characters. The shortest name is 2 characters. Data collected in 2019.
    [StringLength(51, MinimumLength = 2, ErrorMessage = "Artist name is too short or too long")]
    public string Name { get; set; }
  }
}