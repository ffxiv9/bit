﻿using System.Threading.Tasks;
using BandsInTown.Domain.Contracts.Services;
using BandsInTown.WebAPI.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BandsInTown.WebAPI.Controllers
{
  [ApiController]
  [Authorize]
  [Route("api/[controller]")]
  public class HistoryController : ControllerBase
  {
    private readonly IArtistSearchTracker _searchTracker;

    public HistoryController(IArtistSearchTracker searchTracker)
    {
      _searchTracker = searchTracker;
    }

    /// <returns>Artists' names from last 10 searches</returns>
    public async Task<IActionResult> Index()
    {
      var searches = await _searchTracker.GetLastSearches(User.GetLoggedInUserId());
      return new JsonResult(searches);
    }
  }
}