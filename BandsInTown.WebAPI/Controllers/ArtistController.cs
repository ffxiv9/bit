﻿using System.Threading.Tasks;
using BandsInTown.Domain.Contracts.Services;
using BandsInTown.WebAPI.Extensions;
using BandsInTown.WebAPI.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BandsInTown.WebAPI.Controllers
{
  [ApiController]
  [Authorize]
  [Route("api/[controller]")]
  public class ArtistController : ControllerBase
  {
    private readonly IArtistService _artistService;
    private readonly IArtistSearchTracker _searchTracker;

    public ArtistController(IArtistService artistService, IArtistSearchTracker searchTracker)
    {
      _artistService = artistService;
      _searchTracker = searchTracker;
    }

    /// <returns>"The most favourite" artist's place to perform. String in the format "City, Country"</returns>
    [HttpGet("{name}")]
    public async Task<IActionResult> Index([FromRoute] ArtistViewModel model)
    {
      var userId = User.GetLoggedInUserId();

      var venue = await _artistService.GetFavoriteConcertVenue(model.Name);
      await _searchTracker.Push(model.Name, userId);

      if (venue == null) return NotFound();
      return Ok($"{venue.City}, {venue.Country}");
    }
  }
}