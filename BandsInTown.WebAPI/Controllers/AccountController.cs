﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using BandsInTown.Domain.Contracts.Services;
using BandsInTown.WebAPI.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace BandsInTown.WebAPI.Controllers
{
  [ApiController]
  [Route("[controller]/[action]")]
  public class AccountController : ControllerBase
  {
    private readonly IConfiguration _configuration;
    private readonly IUserService _userService;

    public AccountController(IConfiguration configuration, IUserService userService)
    {
      _configuration = configuration;
      _userService = userService;
    }

    [AllowAnonymous]
    [HttpPost]
    public async Task<IActionResult> Login([FromBody] UserViewModel model)
    {
      var user = await _userService.ValidateUserPassword(model.Username, model.Password);
      if (user == null) return Unauthorized();

      var key = _configuration["Jwt:Key"];
      var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));
      var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

      var claims = new[]
      {
        new Claim(JwtRegisteredClaimNames.Sub, user.Id.ToString()),
        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
      };
      var token = new JwtSecurityToken(_configuration["Jwt:Issuer"], _configuration["Jwt:Audience"],
        claims, expires: DateTime.UtcNow.AddMinutes(120), signingCredentials: credentials);
      var tokenHandler = new JwtSecurityTokenHandler().WriteToken(token);
      return Ok(new {token = tokenHandler});
    }
  }
}