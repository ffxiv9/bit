﻿using System;
using System.Security.Claims;

namespace BandsInTown.WebAPI.Extensions
{
  public static class ClaimsPrincipalExtensions
  {
    public static int GetLoggedInUserId(this ClaimsPrincipal principal)
    {
      if (principal == null) throw new ArgumentNullException(nameof(principal));

      return int.Parse(principal.FindFirstValue(ClaimTypes.NameIdentifier));
    }
  }
}