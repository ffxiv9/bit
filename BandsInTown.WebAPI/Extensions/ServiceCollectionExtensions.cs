﻿using System.Net.Http;
using System.Reflection;
using BandsInTown.Data.Contracts.DataProviders;
using BandsInTown.Data.Implementations;
using BandsInTown.Data.Implementations.DataProviders.Bit;
using BandsInTown.Domain.Contracts.Services;
using BandsInTown.Domain.Implementations.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace BandsInTown.WebAPI.Extensions
{
  public static class ServiceCollectionExtensions
  {
    public static IServiceCollection AddDatabase(this IServiceCollection services, IConfiguration configuration)
    {
      return services.AddDbContext<DataContext>(options =>
      {
        options.UseSqlite(configuration.GetConnectionString("DefaultConnection"),
          b => b.MigrationsAssembly(typeof(Startup).GetTypeInfo().Assembly.GetName().Name));
      });
    }

    public static IServiceCollection AddSearchTracking(this IServiceCollection services)
    {
      return services.AddScoped<IArtistSearchTracker, ArtistSearchTracker>();
    }

    public static IServiceCollection AddArtistInformationService(this IServiceCollection services,
      IConfiguration configuration)
    {
      services.AddScoped<IEventDataProvider>(e =>
      {
        var client = e.GetService<IHttpClientFactory>().CreateClient();
        return new EventDataProvider(configuration["BitAppId"], client);
      });
      return services.AddScoped<IArtistService, ArtistService>();
    }

    public static IServiceCollection AddAccounting(this IServiceCollection services)
    {
      return services.AddScoped<IUserService, UserService>();
    }
  }
}